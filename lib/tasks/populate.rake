require 'ffaker'

namespace :db do
  desc "TODO// It generates fake users"
  task populate: :environment do

    User.destroy_all

    def random_roles
      [:customer, :agent].sample
    end

    20.times do
      user = User.new(
        first_name: FFaker::Name.first_name,
        last_name: FFaker::Name.last_name,
        email: FFaker::Internet.email,
        contact: FFaker::PhoneNumber.phone_number
      )
      user.add_role random_roles
      user.password = "click123"
      user.password_confirmation = "click123"  
      user.save!
    end
  end
end

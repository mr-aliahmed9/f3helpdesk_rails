describe Api::V1::BaseController, :type => :controller  do
  describe "GET #tags" do
    before(:each) do
      user = create(:user)

      command = AuthenticateUser.call(user.email, user.password)
      @token, @user = command.result

      request.headers['Authorization'] = @token
    end

    it "gets all the tags" do
      get :tags

      expect(response.body).to look_like_json
      expect(body_as_json["status"]).to eql(200)
    end
  end
end

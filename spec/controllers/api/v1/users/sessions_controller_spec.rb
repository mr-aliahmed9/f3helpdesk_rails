describe Api::V1::Users::SessionsController, :type => :controller  do
  describe "POST #create" do
    before(:each) do
      @user = create(:user)
    end

    context 'when email and password are valid' do
      it "returns user jwt token and user data" do
        post :create, params: { email: @user.email, password: @user.password }
        expect(body_as_json.has_key?(:auth_token)).to eql(true)
        expect(body_as_json.has_key?(:user)).to eql(true)
        expect(body_as_json["status"]).to eql(200)
      end
    end

    context 'when email and password are invalid' do
      it "returns unauthorized error" do
        post :create, params: { email: "", password: "" }

        expect(body_as_json["status"]).to eql(401)
      end
    end
  end
end

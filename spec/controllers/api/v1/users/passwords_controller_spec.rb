describe Api::V1::Users::PasswordsController, :type => :controller  do
  let(:new_user) { create :user }
  let(:user) { User.first || User.find(new_user.id) }

  describe "POST #create" do
    context 'when user attempts for a password request' do
      it "sends password instruction email when user email is present" do
        post :create, params: { email: user.email }
        expect(body_as_json["status"]).to eql(200)
      end

      it "returns error message when user email is blank" do
        post :create, params: { email: "" }
        expect(body_as_json["status"]).to eql(401)
        expect(body_as_json["error"]).to eql("Email can't be blank")
      end
    end
  end

  describe "GET #validates_password_token" do
    context 'when password reset token is invalid' do
      it 'responds with 404' do
        get :validates_password_token, params: { token: "invalid_token" }
        expect(body_as_json["status"]).to eql(404)
        expect(body_as_json["message"]).to eql("Couldn't find User")
      end
    end

    context 'when password reset token is valid' do
      it "checks the the validity of token" do
        user.generate_password_token
        get :validates_password_token, params: { token: user.password_reset_token }
        expect(body_as_json["status"]).to eql(200)
      end
    end
  end

  describe "PUT #update/:token" do
    let(:token) do
      user.generate_password_token
      user.password_reset_token
    end

    let(:agent_params) { FactoryGirl.attributes_for(:is_new_agent) }

    context 'when resource is found' do
      it 'responds with 200' do
        put :update, params: { token: token, user: agent_params }
        expect(body_as_json["status"]).to eql(200)
      end
    end

    context 'when resource is not found' do
      it 'responds with 404' do
        put :update, params: { token: "invlid_token", user: agent_params }
        expect(body_as_json["status"]).to eql(404)
        expect(body_as_json["message"]).to eql("Couldn't find User")
      end
    end

    describe "Update Agent's Account" do
      context 'when params are #valid' do
        it "checks the new user param" do
          put :update, params: { token: token, user: agent_params }
          expect(request.params[:user][:is_new_user]).to eql("true")
        end

        it "updates the agent account" do
          put :update, params: { token: token, user: agent_params }
          expect(body_as_json["message"]).to eql("Your account has been updated.")
        end
      end

      context 'when params are #invalid' do
        let(:invalid_agent_params) { FactoryGirl.attributes_for(:invalid_agent) }

        it "responds with 401" do
          put :update, params: { token: token, user: invalid_agent_params }
          expect(body_as_json["status"]).to eql(401)
        end

        it "returns with error messages" do
          put :update, params: { token: token, user: invalid_agent_params }
          expect(body_as_json["error"].length).to be > 0
        end
      end
    end

    describe "Update User's Password" do
      context 'when params are #valid' do
        let(:user_params) { FactoryGirl.attributes_for(:user) }
        it "updates the user's password" do
          put :update, params: { token: token, user: user_params }
          expect(body_as_json["message"]).to eql("Your password has been changed.")
        end
      end
    end
  end
end

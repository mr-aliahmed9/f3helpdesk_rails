describe Api::V1::Users::RegistrationsController, :type => :controller  do
  describe "POST #create" do

    let(:user_params) { FactoryGirl.attributes_for(:user, :customer) }
    let(:invalid_user_params) { FactoryGirl.attributes_for(:invalid_customer) }

    context 'when params are passed and valid' do
      it "creates a new user" do
        expect{
          post :create, params: { user: user_params }
        }.to change(User, :count).by(1)
      end

      it "returns status 200" do
        post :create, params: { user: user_params }
        expect(body_as_json["status"]).to eql(200)
      end

      it "returns success message" do
        post :create, params: { user: user_params }
        expect(body_as_json["message"]).to eql("Your account has been created.")
      end
    end

    context 'when params are passed and invalid' do
      it "creates a new user with invalid params" do
        expect{
          post :create, params: { user: invalid_user_params }
        }.to change(User, :count).by(0)
      end

      it "returns status 401" do
        post :create, params: { user: invalid_user_params }
        expect(body_as_json["status"]).to eql(401)
      end

      it "returns error messages" do
        post :create, params: { user: invalid_user_params }

        expect(body_as_json["error"].length).to be > 0
      end
    end
  end
end

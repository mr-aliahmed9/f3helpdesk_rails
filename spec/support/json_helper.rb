module JsonHelper
  require 'rails_helper'
  require 'spec_helper'
  
  def body_as_json
    json_str_to_hash(response.body)
  end

  def json_str_to_hash(str)
    JSON.parse(str).with_indifferent_access
  end
end

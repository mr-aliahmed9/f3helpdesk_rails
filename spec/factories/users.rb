FactoryGirl.define do
  sequence :email do |n|
    "f3helpdesk_#{n}@example.com"
  end

  factory :user do
    first_name "John"
    last_name "Smith"
    email
    password "click123"
    password_confirmation "click123"
    contact "+923162683386"

    trait :customer do
      first_name "Example"
      last_name "Customer"
      email
    end

    trait :agent do
      first_name "Example"
      last_name "Agent"
      email
    end

    trait :invalid_user do
      first_name "John"
      last_name ""
      password ""
      password_confirmation "click123"

      contact ""
    end

    factory :is_new_agent do
      agent
      is_new_user true
    end

    factory :invalid_agent do
      invalid_user
      add_role? :agent
    end

    factory :invalid_customer do
      invalid_user
      add_role? :customer
    end
  end
end

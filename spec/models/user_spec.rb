describe User do
  subject {
    create(:user)
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end
  it "is invalid without a firstname" do
    subject.first_name = nil
    expect(subject).to be_invalid
  end
  it "is invalid without a lastname" do
    subject.last_name = nil
    expect(subject).to be_invalid
  end

  describe User, ".full_name" do
    it 'returns full_name of user' do
      full_name = [subject.first_name, subject.last_name].join(" ")
      expect(subject.full_name).to eql(full_name)
    end
  end
end

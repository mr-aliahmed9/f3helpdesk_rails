class CreateTicketAssignments < ActiveRecord::Migration[5.0]
  def change
    create_table :ticket_assignments do |t|
      t.integer :ticket_id
      t.integer :agent_id

      t.timestamps
    end
  end
end

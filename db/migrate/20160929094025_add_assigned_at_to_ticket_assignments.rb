class AddAssignedAtToTicketAssignments < ActiveRecord::Migration[5.0]
  def change
    add_column :ticket_assignments, :assigned_at, :datetime
  end
end

class AddCustomerIdToTicketAssignments < ActiveRecord::Migration[5.0]
  def change
    add_column :ticket_assignments, :customer_id, :integer
  end
end

class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.string :title
      t.text :description
      t.string :priority
      t.integer :status
      t.integer :customer_id

      t.timestamps
    end
  end
end

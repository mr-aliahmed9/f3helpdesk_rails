require 'test_helper'

class NotificationMailerTest < ActionMailer::TestCase
  test "send_password_request" do
    mail = NotificationMailer.send_password_request
    assert_equal "Send password request", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end

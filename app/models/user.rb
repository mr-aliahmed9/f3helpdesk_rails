class User < ApplicationRecord
  rolify

  has_many :tickets, foreign_key: :customer_id
  has_many :assigned_agents, class_name: "TicketAssignment", foreign_key: :customer_id

  has_many :ticket_assignments, class_name: "TicketAssignment", foreign_key: :agent_id
  has_many :assigned_tickets, through: :ticket_assignments, source: :ticket

  attr_accessor :password, :password_confirmation, :setting_password, :is_new_user

  validates_presence_of     :email
  validates_uniqueness_of   :email

  validates_presence_of     :first_name, :last_name, :if => :setting_password

  validates_confirmation_of :password
  validates_presence_of     :password, if: :setting_password
  validates_length_of       :password, :in => 8..12, if: :setting_password
  validates_presence_of     :password_confirmation, if: :should_confirm_password?

  # validates :avatar, attachment_presence: true, :if => :setting_password
  # validates_with AttachmentPresenceValidator, attributes: :avatar, :if => :setting_password
  # validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 1.megabytes, :if => :setting_password

  before_save :encrypt_password
  after_initialize :default_values

  after_create :send_agent_notification

  has_attached_file :avatar, styles: { medium: "300x300#", thumb: "100x100#" }, default_url: "/assets/user-avatar.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  def as_json(options = { })
    h = super(:except => [:password_digest, :password_salt, :password_hash, :password_reset_sent_at, :password_reset_token])
    h[:full_name] = full_name
    h[:role]      = role
    h[:avatar]    = {
      thumb: Rails.application.config.asset_url + avatar.url(:thumb),
      medium: Rails.application.config.asset_url + avatar.url(:medium),
    }
    h
  end

  def nil_pwd_reset_token
    self.password_reset_token = nil
    save!
  end

  def unconfirmed_user?
    !self.password_salt
  end

  def authenticate(password)
		unless self.password_hash == BCrypt::Engine.hash_secret(password, self.password_salt)
      return nil
		end

    self
  end

  def full_name
    name = [first_name, last_name].join(" ")
    name.blank? ? email : name
  end

  def role
    if has_role? :customer
      "Customer"
    else
      "Agent"
    end
  end

  def send_agent_notification
    if has_role? :agent and password.blank?
      send_password_instructions({is_new_user: true})
    end
  end

  def send_password_instructions(options = {})
    generate_password_token()
    options[:token] = @token
    if unconfirmed_user?
      options[:is_new_user] = true
    end

    NotificationMailer.password_setup_request(self, options).deliver_now!
  end

  class << self
    def make_customer(params)
      user = new(params)
      user.add_role :customer
      user
    end
  end


  def generate_password_token
    generate_token(:password_reset_token)
    self.setting_password = false
    self.password_reset_sent_at = Time.now
		save!
  end

  private

  def generate_token(column)
		begin
			self[column] = SecureRandom.urlsafe_base64
		end while User.exists?(column => self[column])

    @token = self[column]
	end

  # If password_confirmation is passed, business as usual.
  # If not, don't run the validations
  def should_confirm_password?
    self.password_confirmation.present? || false
  end

  def encrypt_password
		if self.password.present?
			self.password_salt = BCrypt::Engine.generate_salt
			self.password_hash = BCrypt::Engine.hash_secret(self.password, self.password_salt)
		end
	end

  def default_values
    self.setting_password = true
  end
end

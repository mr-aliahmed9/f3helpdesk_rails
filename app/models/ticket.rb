class Ticket < ApplicationRecord
  acts_as_taggable

  validates_presence_of :title, :description, :priority

  belongs_to :customer, -> { joins(:roles).where("roles.name = 'customer'") }, class_name: "User", foreign_key: :customer_id
  has_one :assignment, class_name: "TicketAssignment", dependent: :destroy

  enum status: { newly: 0, open: 1, in_process: 2, closed: 3 }

  STATUS = { newly: 0, open: 1, in_process: 2, closed: 3 }

  # default_scope { order("created_at desc") }

  %w(high medium low).each do |name|
    scope "#{name}_priority".to_sym, -> { where("priority = ?", name.capitalize) }
  end

  %w(newly open in_process closed).each do |name|
    scope name.to_sym, -> { where("status = ?", STATUS[name.to_sym]) }
  end

  class << self
    def fetch_with_title(query)
      where("title like ?", "%#{query}%")
    end
  end

  def as_json(options = { })
    h = super()
    h[:assigned_agent] = self.assignment.try("agent").try("as_json")
    h[:assignee_customer] = self.try("customer").try("as_json")
    if self.assignment.present? and self.assignment.assigned_at.present?
      assigned_at = self.assignment.assigned_at.strftime('%d %b, %y %I:%M %p')
    end
    h[:assigned_at] = assigned_at.present? ? assigned_at : ""
    h
  end
end

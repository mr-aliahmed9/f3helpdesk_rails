class TicketAssignment < ApplicationRecord
  belongs_to :ticket
  belongs_to :agent, class_name: "User", foreign_key: :agent_id
  belongs_to :customer, class_name: "User", foreign_key: :customer_id

  after_create :update_status, :assigned_date

  def update_status
    self.ticket.update_attribute(:status, Ticket::STATUS[:in_process])
  end

  def assigned_date
    self.update_attribute(:assigned_at, DateTime.now)
  end
end

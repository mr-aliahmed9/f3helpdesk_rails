module Api
  module V1
    class TicketAssignmentController < BaseController
      before_action :set_agent
      before_action :roled_based_authorization, only:[:destroy]

      def create
        @assignent = @agent.ticket_assignments.build(ticket_assignment_params)
        if @assignent.save
          render :json => { status: 200, data: @agent.as_json, message: "You have been assigned", html: "<span class='label label-#{ticket_status[@assignent.ticket.status.to_sym]}'>#{@assignent.ticket.status.humanize}</span>".html_safe}, status: :created
        else
          render :json => { status: :error, message: "Couldn't Assign"}, status: false
        end
      end

      def destroy
        @assignments = @agent.assigned_tickets.where(customer_id: params[:customer_id])
        @assignments.destroy_all
        success_msg = "Ticket Assignment removed."
        render :json => { status: 200, data: @agent.as_json, message: success_msg}, status: :created
      end

      private

      def set_agent
        begin
          @agent = User.find(params[:agent_id])
        rescue ActiveRecord::RecordNotFound
          render :json => { status: false, error: "Record not found"}, status: false and return
        end
      end

      def ticket_assignment_params
        params.require(:ticket_assignment).permit(:ticket_id, :customer_id)
      end
    end
  end
end

module Api
  module V1
    class TicketsController < BaseController
      before_action :set_ticket, only: [:update, :destroy]
      def index
        @tickets = @current_user.tickets
        respond_with @tickets
      end

      def create
        @ticket = @current_user.tickets.build(ticket_params)
        if @ticket.save
          render :json => { status: 200, message: "Ticket has been filed"}, status: :created
        else
          render :json => { status: :error, message: @ticket.errors.full_messages.map { |msg| content_tag(:li, msg) }.join}, status: false
        end
      end

      def update
        if @ticket.update_attributes(ticket_params)
          render :json => { status: 200, message: "Ticket has been updated"}, status: :created
        else
          render :json => { status: :error, message: @ticket.errors.full_messages}, status: false
        end
      end

      def destroy
        begin
          @ticket.destroy
        rescue Exception => e
          logger.debug e.message
          render :json => { status: 500, error: "Something went wrong"}, status: :failed and return
        end
        render :json => { status: 200, message: "Ticket successfully removed"}, status: :created
      end

      private

      def set_ticket
        begin
          @ticket = @current_user.tickets.find(params[:id])
        rescue ActiveRecord::RecordNotFound
          render :json => { status: false, error: "Record not found"}, status: failed and return
        end
      end

      def ticket_params
        params.require(:ticket).permit(:title, :description, :priority, :status, :tag_list => [])
      end
    end
  end
end

module Api
  module V1
    class DashboardController < BaseController
      before_action :roled_based_authorization, only:[:assigned_agents]

      def index
        if @current_user.has_role? :customer
          @tickets = @current_user.tickets.order("created_at desc")
        else
          @agent_tickets = @current_user.assigned_tickets
          @tickets = Ticket.where.not(id: @agent_tickets.map(&:id))
        end

        # begin
          @payload[:json][:data] = []

          if params[:agent_tickets].present?
            scoped_tickets = @agent_tickets.send(params[:scope]) if params[:scope]
          else
            scoped_tickets = @tickets.send(params[:scope]) if params[:scope] and @tickets.present?
          end

          scoped_tickets = scoped_tickets.fetch_with_title(params[:query]) if params[:query]


          if params[:tag_list].present?
            scoped_tickets = scoped_tickets.tagged_with(params[:tag_list], :any => true)
          end

          count_scoped_tickets()

          @payload[:json]["#{params[:scope]}_count".to_sym] = scoped_tickets.length if scoped_tickets.present?

          if scoped_tickets.present?
            @payload[:json][:ticketsData] = scoped_tickets.as_json
          end

        # rescue Exception => e
        #   @payload = {json: { status: false, error: e.message }, status: :forbidden}
        # end

        render :json => @payload[:json], :status => @payload[:status]
      end

      def assigned_agents
        @assined_agents = @current_user.assigned_agents.collect(&:agent).uniq
        @payload[:json][:assigned_agents] = @assined_agents.as_json
        render :json => @payload[:json], :status => @payload[:status]
      end

      def agent_assigned_tickets
        @assigned_tickets = @current_user.assigned_tickets
        @payload[:json][:assigned_tickets] = @assigned_tickets.as_json
        render :json => @payload[:json], :status => @payload[:status]
      end

      def agent_assignees
        @customers = @current_user.assigned_tickets.collect(&:customer).uniq
        @payload[:json][:customers] = @customers.as_json
        render :json => @payload[:json], :status => @payload[:status]
      end

      def pdf_download
        @date = Date.today.beginning_of_month

        if params[:assigned_date].present?
          @date = params[:assigned_date].to_date
        end

        @tickets = @current_user.assigned_tickets.where("assigned_at >= ?", @date)

        if params[:scope].present? and @tickets.present?
          @tickets = @tickets.send(params[:scope])
        end

        respond_to do |format|
          format.pdf do
            pdf  = render_to_string pdf: "ticket_report",
                   file: "#{Rails.root}/public/downloads/tickets.pdf.erb",
                   layout: '/layouts/pdf.html.erb',
                   print_media_type: true,
                   title: "Tickets Report for the month of #{@date.strftime('%B')}",
                   disposition: "download"
            send_data(pdf, :filename => "assigned_report_#{@date.to_s}",  :type => "application/pdf")
          end
        end
      end

      private

      def count_scoped_tickets
        @payload[:json][:high_priority_count]    = @tickets.high_priority.count
        @payload[:json][:medium_priority_count ] = @tickets.medium_priority.count
        @payload[:json][:low_priority_count]     = @tickets.low_priority.count

        @payload[:json][:newly_count]            = @tickets.newly.count
        @payload[:json][:open_count]             = @tickets.open.count

        if @current_user.has_role? :agent
          if @agent_tickets.present?
            @payload[:json][:in_process_count]   = @agent_tickets.in_process.count
            @payload[:json][:closed_count]       = @agent_tickets.closed.count
          else
            @payload[:json][:in_process_count]   = 0
            @payload[:json][:closed_count]       = 0
          end
        else
          @payload[:json][:in_process_count]     = @tickets.in_process.count
          @payload[:json][:closed_count]         = @tickets.closed.count
        end
      end
    end
  end
end

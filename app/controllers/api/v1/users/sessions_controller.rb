module Api
  module V1
    class Users::SessionsController < BaseController
      skip_before_action :authenticate_request!

      # Authenticates user
      # @params -> :email, :password
      # returns user and user JSON web token
      def create
        command = AuthenticateUser.call(params[:email], params[:password])

        if command.success?
          token, user = command.result
          @payload[:json][:auth_token] = token
          @payload[:json][:user] = user.as_json
        else
          @payload[:json][:error] = command.errors[:base]
          @payload[:json][:status] = 401
          @payload[:status] = :unauthorized
        end

        render json: @payload[:json], status: @payload[:status]
      end
    end
  end
end

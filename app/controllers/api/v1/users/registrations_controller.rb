module Api
  module V1
    class Users::RegistrationsController < BaseController
      skip_before_action :authenticate_request!
      before_action :process_with_file_params

      def create
        @user = User.make_customer(user_params)
        if @user.save
          @payload[:json][:message] = "Your account has been created."
          @payload[:json][:status] = 200
        else
          @payload[:json][:error] = @user.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
          @payload[:json][:status] = 401
          @payload[:status] = :unauthorized
        end

        render json: @payload[:json], status: @payload[:status]
      end

      private

      def user_params
        params.require(:user).permit(:first_name, :last_name, :contact, :email, :password, :password_confirmation, :avatar)
      end
    end
  end
end

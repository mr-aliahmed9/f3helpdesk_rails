module Api
  module V1
    class Users::PasswordsController < BaseController
      skip_before_action :authenticate_request!
      before_action :set_user, :check_token_validatity, only: [:update, :validates_password_token]
      before_action :process_with_file_params, only: [:update]

      # Request Password created
      # @params -> :email
      # returns payload
      def create
        if params[:email].present?
          @user = User.find_by_email(params[:email])
          @user.send_password_instructions if @user.present?
        else
          @payload = { json: { status: 401, error: "Email can't be blank"}, status: :unauthorize}
        end

        render :json => @payload[:json], :status => @payload[:status] and return
      end

      # Request Password updated
      # @params -> :user
      # This action checks if the user is agent and new record
      # returns payload
      def update
      	if @payload[:json][:status] == 200
          if password_params[:is_new_user].present?
            @user.setting_password = false
            success_msg = "Your account has been updated."
          else
            success_msg = "Your password has been changed."
          end

          if @user.update_attributes(password_params)
            @user.nil_pwd_reset_token
            @payload[:json][:message] = success_msg
          else
            @payload = { json: { status: 401, error: @user.errors.full_messages.map { |error_msg| content_tag(:li, error_msg) }.join}, status: :unauthorize}
          end
        end

        render :json => @payload[:json], :status => @payload[:status] and return
      end

      # Perform validation check if the password token is sent 2.hours ago
      def validates_password_token
        render :json => @payload[:json], :status => @payload[:status] and return
      end

      private

      def set_user
        begin
          @user = User.find_by_password_reset_token!(params[:token])
        rescue ActiveRecord::RecordNotFound => e
          @payload = { json: { status: 404, message: e.message}, status: :not_found}
        end
      end

      def check_token_validatity
        begin
          if @user and @user.password_reset_sent_at < 2.hours.ago
            @payload = { json: { status: 401, message: "Token has been expired."}, status: :unauthorize}
          end
        rescue Exception => e
          logger.info e.message
          @payload = { json: { status: 403, message: e.message}, status: :forbidden}
        end
      end

      def password_params
        params.require(:user).permit(:first_name, :last_name, :contact, :password, :password_confirmation, :is_new_user, :avatar)
      end
    end
  end
end

module Api
  module V1
    class BaseController < ApplicationController
      before_action :authenticate_request!, :payload
      respond_to :json
      include ApplicationHelper

      # Returns All the Tags added with the tickets
      def tags
        @tags = Array.new

        ActsAsTaggableOn::Tag.all.each do |tag|
          @tags << {
            text: tag.name
          }
        end

        @payload[:json][:data] = @tags
        render :json => @payload[:json], :status => @payload[:status]
      end

      # Verifies if the user is signed in or not.
      def send_authenticated_user
        if @current_user
          @payload[:json][:user] = @current_user.as_json
          @payload[:json][:auth_token] = request.headers["Authorization"]
        end
        render json: @payload[:json], status: 200
      end

      protected

      # Verifies that the user is authenticated to access restricted area
      # @params -> :auth_token (in params or request headers)
      def authenticate_request!
        @current_user = AuthorizeApiRequest.call(request.headers, params).result
        render json: { error: 'Not Authorized User', status: 401 } unless @current_user
      end

      # Default payload to return for every API
      def payload
        @payload = {
          json: { success: true, status: 200 },
          status: :created
        }
      end

      # Process the params which include file
      # This is used for uploading FILE Data by taking requested buffer
      def process_with_file_params
        if params[:user]
          params[:user] = params[:user].as_json.with_indifferent_access

          if params[:file]
            params[:user][:avatar] = params[:file]
          end
        end
      end

      # Authorized user on the basis of their role passed
      def roled_based_authorization
        unless @current_user.has_role?(params[:role].to_sym)
          render json: { error: 'Not Authorized User' }, status: 401
        end
      end
    end
  end
end

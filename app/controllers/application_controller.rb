class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  include ActionView::Helpers::TagHelper
  include ActionView::Context
end

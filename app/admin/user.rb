ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :list, :of, :attributes, :on, :model, :email, :role
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  config.clear_action_items!

  action_item(:index) do
    link_to "New Agent" , "/admin/users/new" if params[:scope] == "agents"
  end

  scope :customers, :default => true do |users|
    users.with_role(:customer)
  end

  scope :agents do |users|
    users.with_role(:agent)
  end


  index do
    selectable_column
    id_column
    column :full_name
    column :email
    column :contact
    actions
  end


  form do |f|
    f.inputs "Agent Details" do
      f.input :email
      f.input :role, :as => :hidden, :input_html => { :value => :agent }
    end
    f.actions do
      f.action :submit, :as => :button, label: 'Send Agent Request'
      f.action :cancel, :as => :link
    end
  end

  controller do
    def new
      @user = User.new
    end

    def create
      @user = User.new(agent_params)
      @user.setting_password = false
      @user.add_role :agent
      if @user.save
        redirect_to admin_users_path, notice: "Agent created. An email has been sent to the agent for password setup."
      else
        flash[:alert] = @user.errors.messages
        render :new
      end
    end

    private

    def agent_params
      params.require(:user).permit(:email)
    end
  end
end

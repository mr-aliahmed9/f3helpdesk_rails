# Generic Class for Authentication
# Encode the Auth Token with the User Id and return
class AuthenticateUser
  prepend SimpleCommand
  def initialize(email, password)
    @email = email
    @password = password
  end

  def call
    if !user
      errors.add :base, I18n.t("devise.failure.not_found_in_database")
      return
    end

    token = JsonWebToken.encode(user_id: user.id)
    [token, user]
  end

  private

  attr_accessor :email, :password

  def user
    user = User.find_by_email(email)
    return user if user && user.authenticate(password)
  end
end

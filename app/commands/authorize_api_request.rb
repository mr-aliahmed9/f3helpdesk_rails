# Generic Class for Authorizing with JWT
# Decode the Auth Token and gets the User Id
class AuthorizeApiRequest
  prepend SimpleCommand
  def initialize(headers = {}, params = {})
    @headers = headers
    @params = params
  end

  def call
    user
  end

  private

  attr_reader :headers, :params

  def user
    @user ||= User.find(decoded_auth_token[:user_id]) if decoded_auth_token
    @user || errors.add(:token, I18n.t("token_auth.invlaid_token")) && nil
  end

  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
  end

  def http_auth_header
    requested_token = headers['Authorization'] || params[:token]
    if requested_token.present?
      return requested_token
    else
      errors.add(:token, I18n.t("token_auth.missing_token"))
    end

    nil
  end
end

module ApplicationHelper
  def ticket_status
    {
      :newly => "default",
      :open => "info",
      :in_process => "warning",
      :closed => "danger"
    }
  end
end

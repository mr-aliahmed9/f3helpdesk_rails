class ApplicationMailer < ActionMailer::Base
  default from: 'aliahmed@folio3.com'
  layout 'mailer'

  include MailerModules::MailerUrl
end

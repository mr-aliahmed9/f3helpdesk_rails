# This module is extended to application mailer class
# this provides all the urls that should be served on angular side
module MailerModules
  module MailerUrl
    def password_setup_request_url(options)
      query_string = options.to_query

      url = ""
      url << host + "/#/users/setup_password?"
      url << query_string
      url
    end

    private

    def host
      Rails.application.config.client_side_host
    end
  end
end

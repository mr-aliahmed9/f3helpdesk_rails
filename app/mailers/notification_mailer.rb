class NotificationMailer < ApplicationMailer
  def password_setup_request(agent, token)
    @greeting = "Hi Agent"
    @url = password_setup_request_url(token)
    mail to: agent.email, subject: "New Sign Up | Setup Password"
  end

  def password_instructions(user, token)
    @greeting = "Hi"
    @url = password_setup_request_url(token)
    mail to: user.email, subject: "Reset Password Instructions"
  end
end

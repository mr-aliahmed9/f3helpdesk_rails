Rails.application.routes.draw do
  devise_for :admins, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  namespace :api, defaults: {format: :json} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      scope module: :users do
        resources :sessions, only: [:create, :delete]
        resources :registrations, only: [:create, :delete]
        resources :passwords, only: [:create] do
          collection do
            put "/:token" => "passwords#update"
            get "validates_password_token"
          end
        end
      end

      get "tags" => "base#tags"
      get "send_authenticated_user" => "base#send_authenticated_user"

      get "dashboard" => "dashboard#index"
      get "/assigned_agents/:role" => "dashboard#assigned_agents"
      get "/assigned_tickets/:role" => "dashboard#agent_assigned_tickets"
      get "/my_customers/:role" => "dashboard#agent_assignees"
      get "/assignment_report" => "dashboard#pdf_download", format: :pdf

      resources :tickets
      resources :ticket_assignment, except: [:destroy] do
        delete "/:agent_id/:customer_id/:role" => "ticket_assignment#destroy", on: :collection
      end
    end
  end

  root to: "admin/dashboard#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
